<?php

namespace App\Repository;

use App\Entity\HttpLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HttpLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method HttpLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method HttpLog[]    findAll()
 * @method HttpLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HttpLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HttpLog::class);
    }

    public function findLastLogs($limit = 20): array
    {
        return $this->createQueryBuilder('http_log')
            ->orderBy('http_log.id', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }
}
