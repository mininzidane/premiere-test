<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelInterface;

class ResponseListener
{
    private const PARAM_REQUEST_LOG = 'request-log';

    private KernelInterface $kernel;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(KernelInterface $kernel, EventDispatcherInterface $eventDispatcher)
    {
        $this->kernel = $kernel;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if (!\in_array($this->kernel->getEnvironment(), ['prod', 'dev'], true)) {
            return;
        }

        if (!$event->isMainRequest()) {
            return;
        }

        $response = $event->getResponse();
        $request = $event->getRequest();

        $captureRequest = $request->get(self::PARAM_REQUEST_LOG);
        if ($captureRequest !== null) {
            $this->eventDispatcher->dispatch(new RequestEvent($request, $response));
        }
    }
}
