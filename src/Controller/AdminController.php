<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\HttpLogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends AbstractController
{
    public function httpLog(HttpLogRepository $httpLogRepository): Response
    {
        $httpLogs = $httpLogRepository->findLastLogs();

        return $this->render('admin/http-log.html.twig', ['httpLogs' => $httpLogs]);
    }
}
