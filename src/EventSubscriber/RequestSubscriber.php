<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\HttpLog;
use App\Event\RequestEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RequestSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onRequest',
        ];
    }

    public function onRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        $httpLog = new HttpLog();
        $httpLog
            ->setRequestUrl($request->getRequestUri())
            ->setClientIp($request->getClientIp())
            ->setRequestHeaders($request->headers->all())
            ->setRequestBody($request->getContent() ?: json_encode($request->request->all()))
            ->setHttpCode($response->getStatusCode())
            ->setResponseBody($response->getContent())
            ->setResponseHeaders($response->headers->all())
        ;
        $this->entityManager->persist($httpLog);
        $this->entityManager->flush();
    }
}
