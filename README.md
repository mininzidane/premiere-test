# README #

## Pre-launch steps

1. Run `docker-compose up -d --build`
    - run `docker-compose exec php composer i`
    - run `docker-compose exec php bin/console doctrine:database:create`
    - run `docker-compose exec php bin/console doctrine:migrations:migrate`
2. Simple homepage is available on http://localhost:8013/
3. Log list page is available on http://localhost:8013/admin/http-log
4. There is unit test for request subscriber. Execute by: `docker-compose exec php bin/phpunit tests`
