<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Event\RequestEvent;
use App\EventSubscriber\RequestSubscriber;
use App\Repository\HttpLogRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class RequestSubscriberTest extends KernelTestCase
{
    /**
     * @var RequestSubscriber|object|null
     */
    private $requestSubscriber;
    /**
     * @var HttpLogRepository|object|null
     */
    private $httpLogRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->requestSubscriber = static::getContainer()->get(RequestSubscriber::class);
        $this->httpLogRepository = static::getContainer()->get(HttpLogRepository::class);
    }

    public function testOnRequest(): void
    {
        $request = $this->createMock(Request::class);
        $url = 'http://some.url';
        $ip = '128.128.129.129';
        $requestHeaders = ['user-agent' => ['Mozilla\/5.0 (X11; Linux x86_64)'], 'cache-control' => ['no-cache']];
        $requestContent = '{"test":"some","test2":"value","test3":"here"}';
        $header = $this->createMock(HeaderBag::class);
        $header->expects($this->once())->method('all')->willReturn($requestHeaders);
        $request->headers = $header;

        $request->expects($this->once())->method('getRequestUri')->willReturn($url);
        $request->expects($this->once())->method('getRequestUri')->willReturn($url);
        $request->expects($this->once())->method('getContent')->willReturn($requestContent);
        $request->expects($this->once())->method('getClientIp')->willReturn($ip);

        $statusCode = 400;
        $responseContent = '<p>Some text here</p>';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())->method('getStatusCode')->willReturn($statusCode);
        $response->expects($this->once())->method('getContent')->willReturn($responseContent);

        $responseHeaders = json_decode('{ "cache-control": [ "no-cache, private" ], "date": [ "Wed, 07 Jul 2021 11:44:29 GMT" ] }', true);
        $responseHeader = $this->createMock(ResponseHeaderBag::class);
        $responseHeader->expects($this->once())->method('all')->willReturn($responseHeaders);
        $response->headers = $responseHeader;

        $requestEvent = $this->createMock(RequestEvent::class);
        $requestEvent->method('getRequest')->willReturn($request);
        $requestEvent->method('getResponse')->willReturn($response);

        $this->requestSubscriber->onRequest($requestEvent);

        $httpLog = $this->httpLogRepository->findOneBy([
            'requestUrl' => $url,
            'clientIp' => $ip,
        ]);
        self::assertNotNull($httpLog);
        self::assertSame($requestContent, $httpLog->getRequestBody());
        self::assertSame($requestHeaders, $httpLog->getRequestHeaders());
        self::assertSame($responseContent, $httpLog->getResponseBody());
        self::assertSame($responseHeaders, $httpLog->getResponseHeaders());
    }
}
